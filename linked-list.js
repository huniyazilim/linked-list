var LinkedList = function () {
  this.head = null;
  this.tail = null;
}

var Node = function (value, next, prev) {
  this.value = value;
  this.next = next;
  this.prev = prev;
}

LinkedList.prototype.addToHead = function (value) {
  var newNode = new Node(value, this.head, null);
  if (this.head){ // listede öğe mevcut ise...
    // yeni nodu başa eklemeden önce, mevcut head nodunun öncesindeki nodu yeni nod olarak belirliyoruz.
    this.head.prev = newNode;
  } else {
    this.tail = newNode;
  }
  this.head = newNode;
};

LinkedList.prototype.addToTail = function (value) {
  var newNode = new Node(value, null, this.tail);
  if (this.tail) { // listede öğe mevcut ise...
    this.tail.next = newNode; // yeni nod tail olmadan önce mevcut tailin nextini yeni nod olarak belirle.
  } else {
    this.head = newNode;
  }
  this.tail = newNode;
};

LinkedList.prototype.removeHead = function () {
  if (!this.head) { // liste boş ise...
    return null;
  }
  var val = this.head.value;
  this.head = this.head.next; // artık bir sonraki node head oldu.
  // yeni belirlediğimiz head nodunun next ve previous özelliklerini ayarla.
  if (this.head) { // head nodu çıkartıldıktan sonra listede başka nod kalmış ise.
    this.head.prev = null; // head ise öncesinde herhangi birşey olmaması gerekir.
  } else { // head nodu çıkartıldıktan sonra listede hiç nod kalmamışsa.
    this.tail = null;
  }
  return val;
};

LinkedList.prototype.removeTail = function () {
  if (!this.tail){
    return null;
  }

  var val = this.tail.value;
  this.tail = this.tail.prev;

  if (this.tail){
    this.tail.next = null; // Artık yeni tail bu nod olduğundan bir sonraki nod diye birşey yok.
  } else {
    this.head = null; // Geride başka nod kalmadıysa head nodu diye birşey de yok!
  }
  return val;
};

LinkedList.prototype.search = function (searchValue) {
  var currentNode = this.head;
  while(currentNode){
    if (currentNode.value === searchValue){
      return currentNode.value;
    }
    currentNode = currentNode.next;
  }
  return null;
};

LinkedList.prototype.toArray = function () {
  var currentNode = this.head;
  var arr = [];

  while(currentNode){
    arr.push(currentNode.value);
    currentNode = currentNode.next;
  }

  return arr;
};

LinkedList.prototype.indexOf = function (searchValue) {
  var currentNode = this.head;
  var idx = 0;
  var idxArr = [];

  while(currentNode){
    if (currentNode.value === searchValue){
      idxArr.push(idx);
    }
    idx = idx + 1;
    currentNode = currentNode.next;
  }

  return idxArr;
};

var LL = new LinkedList();
LL.addToHead(1);
LL.addToHead(2);
LL.addToHead(3);
LL.addToTail(100);
LL.addToTail(200);
LL.addToTail(200);
LL.addToHead(200);

console.log(LL);
console.log(LL.tail.prev.value);

// console.log(LL.removeHead());
// console.log(LL.removeHead());
//
// console.log(LL.removeTail());

console.log(LL.search(200));
console.log(LL.search(2));

console.log(LL.indexOf(200));

LL.indexOf(200).forEach(function (item, idx, arr) {
  console.log(item, LL.toArray()[item]);
});


console.log(binarySearch(LL.toArray(),200));
